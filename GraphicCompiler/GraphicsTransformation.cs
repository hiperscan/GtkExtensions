// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;


namespace Hiperscan.GtkExtensions.Compiler
{
    
    public abstract class GraphicsTransformation
    {
    }
    
    public class GraphicsTranslate : GraphicsTransformation
    {
        public GraphicsTranslate() : this(0.0, 0.0)
        {
        }
        
        public GraphicsTranslate(double x_offset, double y_offset)
        {
            this.XOffset = x_offset;
            this.YOffset = y_offset;
        }
        
        public double XOffset{get;set;}
        public double YOffset{get;set;}
    }
    
    public class GraphicsRotate : GraphicsTransformation
    {
        public GraphicsRotate():this(0)
        {
        }
        
        public GraphicsRotate(double angle_degree)
        {
            this.AngleDegree = angle_degree;
        }
        
        public double AngleDegree{get;set;}
    }
    
    public class GraphicsScale : GraphicsTransformation
    {
        public GraphicsScale() : this(1.0, 1.0)
        {
        }
        
        public GraphicsScale(double x_scale, double y_scale)
        {
            this.XScale = x_scale;
            this.YScale = y_scale;
        }
        
        public double XScale{get;set;}
        public double YScale{get;set;}
    }
}

