// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;


namespace Hiperscan.GtkExtensions.Compiler
{
    #region geometry
    
    public class GraphicsPoint
    {
        public GraphicsPoint():this(0.0, 0.0)
        {
        }
        
        public GraphicsPoint(GraphicsPoint point):this(point.X, point.Y)
        {
        }
        
        public GraphicsPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
        
        public double X{get;set;}
        public double Y{get;set;}
    }
    
    public class GraphicsSize
    {
        public GraphicsSize():this(0.0, 0.0)
        {
        }
        
        public GraphicsSize(GraphicsSize size):this(size.Width, size.Height)
        {
        }
        
        public GraphicsSize(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }
        
        public double Width {get;set;}
        public double Height {get;set;}
    }
    
    public class GraphicsRectangle
    {
        public GraphicsRectangle() : this(0.0, 0.0, 0.0, 0.0)
        {
        }
        
        public GraphicsRectangle(GraphicsRectangle rect) : this(rect.X, rect.Y, rect.Width, rect.Height)
        {
        }
        
        public GraphicsRectangle(GraphicsPoint point, GraphicsSize size) : this(point.X, point.Y, size.Width, size.Height)
        {
        }
        
        public GraphicsRectangle(GraphicsPoint point, double width, double height) : this(point.X, point.Y, width, height)
        {
        }
        
        public GraphicsRectangle(double x, double y, GraphicsSize size) : this(x, y, size.Width, size.Height)
        {
        }
        
        public GraphicsRectangle(double x , double y, double width, double height)
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }
        
        public double X {get;set;}
        public double Y {get;set;}
        public double Width {get;set;}
        public double Height {get;set;}
    }
    
    #endregion geometry
    
    
    #region font
    
    public enum GraphicsFontWeights
    {
        Normal,
        Bold,
    }
    
    public class GraphicsFont
    {
        public GraphicsFont()
        {
            this.FontName = "Helvetica";
            this.FontWeight = GraphicsFontWeights.Normal;
            this.FontSize = 12.0;
        }
        public string FontName {get;set;}
        public GraphicsFontWeights FontWeight {get;set;}
        public double FontSize{get;set;}
    }
    
    #endregion font
    
    #region colors and brushes and pens

    public enum GraphicsColors
    {
        Black,
        Blue,
        Green,
        Red,
        White,    
    }
    
    
    public class GraphicsColor
    {
        public GraphicsColor() : this(0.0, 0.0, 0.0, 1.0)
        {
        }
        
        public GraphicsColor(double red, double green , double blue) : this(red, green, blue, 1.0)
        {
        }
        
        public GraphicsColor(double red, double green , double blue, double alpha)
        {
            this.R = red;
            this.G = green;
            this.B = blue;
            this.A = alpha;
        }
        
        public GraphicsColor(GraphicsColors color)
        {
            switch(color)
            {
            case GraphicsColors.Black:
                this.R = 0.0;
                this.G = 0.0;
                this.B = 0.0;
                this.A = 1.0;
                break;
                
            case GraphicsColors.Blue:
                this.R = 0.0;
                this.G = 0.0;
                this.B = 1.0;
                this.A = 1.0;
                break;
                
            case GraphicsColors.Green:
                this.R = 0.0;
                this.G = 1.0;
                this.B = 0.0;
                this.A = 1.0;
                break;
                
            case GraphicsColors.Red:
                this.R = 1.0;
                this.G = 0.0;
                this.B = 0.0;
                this.A = 1.0;
                break;
                
            case GraphicsColors.White:
                this.R = 1.0;
                this.G = 1.0;
                this.B = 1.0;
                this.A = 1.0;
                break;
            }
        }
        
        public double R {get;set;}
        public double G {get;set;}
        public double B {get;set;}
        public double A {get;set;}
    }
    
    public abstract class GraphicsBrush
    {
        private GraphicsRectangle bounding_box = null;
        public GraphicsRectangle BoundingBox { 
            get {
                if (this.bounding_box == null)
                    throw new Exception ("GraphicsBrush - BoundingBox property is not set.");
                return this.bounding_box;
            }
            set {
                if (value == null)
                    throw new Exception ("GraphicsBrush - BoundingBox property don't access null value.");
                this.bounding_box = value;
            }
        }
    }
    
    public class GraphicsSolidColorBrush : GraphicsBrush
    {
        
        public GraphicsSolidColorBrush() : this(new GraphicsColor())
        {
        }
        
        public GraphicsSolidColorBrush(GraphicsColors color) : this (new GraphicsColor(color))
        {
            
        }
        
        public GraphicsSolidColorBrush(GraphicsColor color)
        {
            this.BoundingBox = new GraphicsRectangle();
            this.Color = color;
        }
        
        
        public GraphicsColor Color {get;set;}
    }
    
    public class GraphicsColorStop
    {
        public GraphicsColorStop() : this (0.0, new GraphicsColor(0, 0, 0, 0))
        {
        }
        
        public GraphicsColorStop(double offset, GraphicsColor color)
        {
            this.Offset = offset;
            this.Color = color;
        }
        
        
        public double Offset {get;set;}
        public GraphicsColor Color {get;set;}
    }
    
    public class GraphicsLinearGradientenBrush : GraphicsBrush
    {
        public GraphicsLinearGradientenBrush()
        {
            this.Start = new GraphicsPoint(0, 0.5);
            this.End = new GraphicsPoint(1, 0.5);
            this.ColorStop1 = new GraphicsColorStop();
            this.ColorStop2 = new GraphicsColorStop();
            this.BoundingBox = new GraphicsRectangle();
        }
        
        public GraphicsPoint Start {get;set;}
        public GraphicsPoint End {get;set;}
        public GraphicsColorStop ColorStop1 {get;set;}
        public GraphicsColorStop ColorStop2 {get;set;}
    }
    
    public class GraphicsRadialGradientenBrush : GraphicsBrush
    {
        public GraphicsRadialGradientenBrush()
        {
            this.Radius1 = 0;
            this.Radius2 = 1;
            this.ColorStop1 = new GraphicsColorStop();
            this.ColorStop2 = new GraphicsColorStop();
        }
        
        public GraphicsColorStop ColorStop1 {get;set;}
        public GraphicsColorStop ColorStop2 {get;set;}
        public double Radius1 {get;set;}
        public double Radius2 {get;set;}
    }
        
    
    public class GraphicsPen
    {
        public GraphicsPen() : this(new GraphicsColor(), 1.0)
        {
        }
        
        public GraphicsPen(GraphicsColor color) : this(color, 1.0)
        {
        }
        
        public GraphicsPen(GraphicsColors color) : this(new GraphicsColor(color), 1.0)
        {
        }
        
        public GraphicsPen(GraphicsColors color, double line_width) : this (new GraphicsColor(color), line_width)
        {
        }
        
        public GraphicsPen(GraphicsColor color, double line_width)
        {
            this.Color = color;
            this.LineWidth = line_width;
        }
        
        public GraphicsColor Color {get;set;}
        public double LineWidth{get;set;}
    }
    
    #endregion colors and brushes and pens
}