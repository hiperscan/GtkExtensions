// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2018 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Gtk;
using Glade;
using System.Drawing;
using System;
using System.Drawing.Drawing2D;

namespace Hiperscan.GtkExtensions.Widgets
{
    public partial class ExtToolTip
    {
        [Widget] Gtk.Container parent_container = null;
        [Widget] Gtk.Button close_button = null;
        [Widget] Gtk.Label spacer = null;
        [Widget] Gtk.Label spacer_left = null;
        [Widget] Gtk.Label spacer_right = null;
        [Widget] Gtk.Alignment close_button_alignment = null;
        [Widget] Gtk.LinkButton linkbutton = null;
        [Widget] Gtk.Label title_label = null;
        [Widget] Gtk.Label content_label = null;
        [Widget] Gtk.Image image = null;

        private int tail_left = 30;
        private int tail_height = 30;
        private int tail_width = 60;
        private int round_rect_angle = 15;
        private static ExtToolTip instance;
        private Color color1 = Color.FromArgb(251, 254, 255);
        private Color color2 = Color.FromArgb(229, 229, 239);


        private void Build(bool is_dialog, string link_txt, EventHandler link_event_handler)
        {
            Glade.XML gui = new Glade.XML(null, "ext_tooltip.glade", "tmp_window", "Hiperscan");
            gui.Autoconnect(this);

            this.parent_container.Unparent();
            this.Add(parent_container);

            this.close_button.UseUnderline = true;

            this.spacer.HeightRequest = tail_height;
            this.spacer_left.WidthRequest = round_rect_angle;
            this.spacer_right.WidthRequest = round_rect_angle;

            if (is_dialog == true)
            {
                this.Modal = true;
                this.close_button_alignment.Xalign = 0f;
                this.close_button_alignment.Xscale = 0f;
                this.close_button_alignment.Yalign = 0.5f;
                this.close_button_alignment.Yscale = 0f;
                this.close_button.Visible = true;
                Gtk.RcStyle style = new Gtk.RcStyle();
                style.Xthickness = 0;
                style.Ythickness = 0;
                this.close_button.ModifyStyle(style);
            }
            else
            {
                this.close_button.NoShowAll = true;
                this.close_button.Visible = false;
                instance = this;
            }

            if (String.IsNullOrEmpty(link_txt) == false)
            {
                this.linkbutton.Label = link_txt;
                this.linkbutton.Clicked += link_event_handler;
                this.linkbutton.Clicked += (sender, e) =>
                {
                    this.Destroy();
                };
                this.linkbutton.Visible = true;
            }
            else
            {
                this.linkbutton.Visible = false;
            }

            this.WindowPosition = WindowPosition.CenterOnParent;
            this.Decorated = false;
            this.SkipPagerHint = true;
            this.SkipTaskbarHint = true;
            this.FocusOnMap = false;

            this.AppPaintable = true;
            this.TypeHint = Gdk.WindowTypeHint.Tooltip;

            this.Child.ShowAll();
            this.DefaultWidth = 583;
            this.DefaultHeight = 257;
            this.Hide();
            this.SizeAllocated += this.OnSizeAllocated;
            this.close_button.Clicked += this.OnCloseDialogButtonClicked;
            this.close_button.KeyPressEvent += this.OnCloseDialogButtonKeyPressed;
        }

        private void OnSizeAllocated(object o, Gtk.SizeAllocatedArgs args)
        {
            Bitmap bmp = new Bitmap(Allocation.Width, Allocation.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;

            GraphUtil g2 = new GraphUtil(g);
            Brush b = new SolidBrush(Color.Red);

            //
            g2.FillRoundRectangle(b, 0, tail_height, bmp.Width, bmp.Height - tail_height, round_rect_angle);
            Point[] points = new Point[]
            {
                new Point( this.tail_left  , this.tail_height ),
                new Point( this.tail_left  , 0           ),
                new Point( this.tail_width , this.tail_height )
            };

            g.FillPolygon(b, points);
            g.Dispose();

            this.ModifyWindowShape(bmp, this);
        }

        private void ModifyWindowShape(System.Drawing.Bitmap bmp, Gtk.Window window)
        {
            try
            {
                //save bitmap to stream
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                //verry important: put stream on position 0
                stream.Position = 0;
                //get the pixmap mask
                Gdk.Pixbuf buf = new Gdk.Pixbuf(stream, bmp.Width, bmp.Height);
                Gdk.Pixmap map1, map2;
                buf.RenderPixmapAndMask(out map1, out map2, 255);
                //shape combine window 
                window.ShapeCombineMask(map2, 0, 0);
                //dispose
                buf.Dispose();
                map1.Dispose();
                map2.Dispose();
                bmp.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\r\n" + ex.StackTrace);
            }
        }

        protected override bool OnExposeEvent(Gdk.EventExpose evnt)
        {
            Graphics g = Gtk.DotNet.Graphics.FromDrawable(evnt.Window);

            Rectangle rect = new Rectangle(Allocation.X, Allocation.Y, Allocation.Width, Allocation.Height);
            Brush b = new LinearGradientBrush(rect, this.color1, this.color2, 90, true);
            g.FillRectangle(b, rect);

            //draw border
            SolidBrush bb = new SolidBrush(Color.FromArgb(147, 148, 166));
            GraphUtil g2 = new GraphUtil(g);
            Pen border_pen = new Pen(bb, 1F);
            g2.DrawRoundRectangle(border_pen, 0, this.tail_height, Allocation.Width - 2, Allocation.Height - tail_height - 1, round_rect_angle);

            //draw tail background again to hide the bottom line

            Point[] points = new Point[]
            {
                new Point( this.tail_left  , this.tail_height + 1 ),
                new Point( this.tail_left  , 0           ),
                new Point(this.tail_width , this.tail_height + 1 )
            };
            g.FillPolygon(b, points);

            //draw tail border
            points = new Point[]
            {
                new Point( this.tail_left - 1 , this.tail_height ),
                new Point( this.tail_left     , 1           ),
                new Point( this.tail_width -1 , this.tail_height )
            };
            g.DrawLine(border_pen, points[0], points[1]);
            g.DrawLine(border_pen, points[1], points[2]);
            return base.OnExposeEvent(evnt);
        }
    }

}
