﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2018 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public partial class ExtToolTip
    {
        private class ToolTipData
        {
            public string Title { get; set; } = String.Empty;
            public string Text { get; set; } = String.Empty;
            public Color Color1 { get; set; } = Color.FromArgb(251, 254, 255);
            public Color Color2 { get; set; } = Color.FromArgb(229, 229, 239);
            public string StockIcon { get; set; } = Stock.DialogInfo;


            public ToolTipData(String title, String text, Color color1, Color color2, String stock)
            {
                this.Title = title;
                this.Text = text;
                this.Color1 = color1;
                this.Color2 = color2;
                this.StockIcon = stock;
            }

            public ToolTipData(String title, String text, Color color1, Color color2)
            {
                this.Title = title;
                this.Text = text;
                this.Color1 = color1;
                this.Color2 = color2;
            }

            public ToolTipData(String title, String text)
            {
                this.Title = title;
                this.Text = text;
            }

            public ToolTipData(String title, String text, String stock)
            {
                this.Title = title;
                this.Text = text;
                this.StockIcon = stock;
            }
        }
    }
}