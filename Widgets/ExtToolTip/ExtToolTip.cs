// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2018 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public partial class ExtToolTip : Gtk.Window
    {
        private static int toolTipInterval = 1500;
        private static bool is_interval_started = false;
        private static Gtk.Widget CurrentWidget = null;
        //
        private static Dictionary<Gtk.Widget,ToolTipData> widgets = new Dictionary<Gtk.Widget,ToolTipData>();
            
        private ExtToolTip() : base(Gtk.WindowType.Popup)
        {
            this.Build(false, null, null);
        }
        
        
        private ExtToolTip(bool is_dialog) : base(Gtk.WindowType.Popup)
        {
            this.Build(is_dialog, null, null);
        }
        
        private ExtToolTip(bool is_dialog, string link_text, EventHandler link_event_handler) :
            base (Gtk.WindowType.Popup)
        {  
            this.Build(is_dialog, link_text, link_event_handler);
        }
        
        private void Close()
        {
            ExtToolTip.instance.Visible = false;
            ExtToolTip.instance.Hide();
        }
        
        private void OnCloseDialogButtonClicked (object sender, System.EventArgs e)
        {
            this.Destroy();
        }
        
        private void OnCloseDialogButtonKeyPressed (object o, Gtk.KeyPressEventArgs args)
        {
            if(args.Event.Key == Gdk.Key.Escape)
                this.Destroy();
        }

        private static ExtToolTip Instance
        {
            get
            {
                if (ExtToolTip.instance == null )
                    ExtToolTip.instance = new ExtToolTip();
                return instance;
            }
        }

        private String ToolTipText
        {
            get
            {
                return this.content_label.Text;
            }
            set
            {
                this.content_label.Text = value;
            }
        }
        
        private String ToolTipTitle
        {
            get
            {
                return this.title_label.Text;
            }
            set
            {
                this.title_label.Markup = "<b>" + value + "</b>";
            }
        }

        private static int ToolTipInterval 
        {
            get 
            {
                return toolTipInterval;
            }
            set
            {
                toolTipInterval = value;
            }
        }

        private string Image
        {
            get 
            {
                return this.image.Stock;
            }
            set 
            {
                this.image.Stock = value;
            }
        }

        public static void SetToolTip( Gtk.Widget widget, String title, String text, String StockIcon )
        {
            ToolTipData data = new ToolTipData( title, text, StockIcon );
            ExtToolTip.AddWidget( widget, data );
        }
        
        public static void SetToolTip( Gtk.Widget widget, String title, String text, Color color1, Color color2)
        {
            ToolTipData data = new ToolTipData( title, text, color1, color2  );
            ExtToolTip.AddWidget(widget, data);
        }
        
        public static void SetToolTip( Gtk.Widget widget, String title, String text )
        {
            ToolTipData data = new ToolTipData( title, text );
            ExtToolTip.AddWidget(widget, data);
        }
        
        public static void SetToolTip( Gtk.Widget widget, String title, String text, Color color1, Color color2, String stock_icon )
        {
            ToolTipData data = new ToolTipData( title, text, color1, color2, stock_icon );
            ExtToolTip.AddWidget(widget, data);
        }    
        
        public static void UnsetToolTip( Gtk.Widget widget)
        {
            if (widgets.ContainsKey(widget))
                widgets.Remove(widget);
        }
        
        public static void ShowToolTipAsDialog(Gtk.Widget widget, string title, string text, string stock_icon )
        {
            int x = 0;
            int y = 0;
            widget.Screen.Display.GetPointer(out x, out y);
            ExtToolTip dialog = new ExtToolTip(true); 
            x -= dialog.tail_left;
            dialog.Move( x , y);
            dialog.ToolTipTitle = title;
            dialog.ToolTipText  = text ;
            dialog.Image    = stock_icon;
            dialog.Resize( 150, 100 );
            dialog.Show();
        }
        
        public static void ShowToolTipAsDialogWithLink(Gtk.Widget widget, string title, string text, string stock_icon,
                                                       string link_text, EventHandler event_handler)
        {
            int x = 0;
            int y = 0;
            widget.Screen.Display.GetPointer(out x, out y); 
        
            ExtToolTip dialog = new ExtToolTip(true, link_text, event_handler);
            x -= dialog.tail_left;
            dialog.Move( x , y);
            dialog.ToolTipTitle = title;
            dialog.ToolTipText  = text ;
            dialog.Image    = stock_icon;
            dialog.Resize( 150, 100 );
            dialog.Show();
        }
        
        
        private static void AddWidget( Gtk.Widget widget, ToolTipData data )
        {
            if( !widgets.ContainsKey( widget ) )
            {
                widgets.Add( widget, data );
                widget.DeleteEvent  += on_widget_delete;
                widget.AddEvents( (int)Gdk.EventMask.ProximityInMask   );
                widget.AddEvents( (int)Gdk.EventMask.PointerMotionMask );
                widget.AddEvents( (int)Gdk.EventMask.PointerMotionHintMask );
                widget.AddEvents( (int)Gdk.EventMask.LeaveNotifyMask  );
                widget.AddEvents( (int)Gdk.EventMask.AllEventsMask    );
                widget.AddEvents( (int)Gdk.EventMask.EnterNotifyMask  );
                widget.AddEvents( (int)Gdk.EventMask.FocusChangeMask  );

                
                widget.WidgetEvent += delegate(object sender, Gtk.WidgetEventArgs args)
                {
                    if( args.Event.Type == Gdk.EventType.LeaveNotify)
                    {
                        is_interval_started = false;
                        Instance.Close();
                    }
                    if( args.Event.Type == Gdk.EventType.EnterNotify )
                    {
                        CurrentWidget       = (Gtk.Widget)sender;
                        is_interval_started = true;
                        GLib.Timeout.Add( (uint)ToolTipInterval, new GLib.TimeoutHandler( ShowMe ) );
                    }                    
                };
            }
            else
            {
                widgets[widget] = data;
            }
        }

        
        private static void CloseAt()
        {
            if (Instance.Visible)
                Instance.Close();
        }
        
        private static bool ShowMe()
        {
            if( !Instance.Visible  && is_interval_started && widgets.ContainsKey(CurrentWidget) )
            {
                Gtk.Widget w         = CurrentWidget;
                ToolTipData data     = widgets[ w ];
                int x = 0;
                int y = 0;
                w.Screen.Display.GetPointer(out x, out y); 

                x -= Instance.tail_left;
                Instance.Move( x , y );
                Instance.ToolTipTitle = data.Title;
                Instance.ToolTipText  = data.Text ;
                Instance.color1       = data.Color1;
                Instance.color2       = data.Color2;
                Instance.Image    = data.StockIcon;
                Instance.Resize( 150, 100 );
                Instance.Show();
            }
            return false;
        }
        
                
        private static void on_widget_delete( object sender, EventArgs args )
        {
            widgets.Remove( (Gtk.Widget) sender );
        }

    }
}
