﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public static class WidgetColor
    {



        public static void SetFontColor(Gtk.Widget widget, Gdk.Color color)
        {
            Gtk.Label label = widget as Gtk.Label;

            if (label == null)
            {
                Gtk.Container container = widget as Gtk.Container;
                if (container == null)
                    return;

                foreach (Widget child in container.Children)
                    WidgetColor.SetFontColor(child, color);
            }

            if (label != null)
                label.ModifyFg(StateType.Normal, color);
        }
    }
}
