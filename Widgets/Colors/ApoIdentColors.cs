﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Gdk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public static class ApoIdentColors
    {
        public static readonly Gdk.Color HIPERSCAN_RED = new Color(192, 30, 30);
        public static readonly Gdk.Color WHITE = new Color(255, 255, 255);
        public static readonly Gdk.Color GRAY = new Color(160, 160, 160);
        public static readonly Gdk.Color lIGHT_GRAY = new Color(228, 228, 228);

        public static readonly Gdk.Color ERROR_BORDER = new Color(255, 160, 160);
        public static readonly Gdk.Color ERROR_FOCUSED_BORDER = new Color(231, 114, 114);
        public static readonly Gdk.Color OK_BORDER = new Color(192, 192, 255);
        public static readonly Gdk.Color OK_FOCUSED_BORDER = new Color(137, 137, 231);

        public static readonly Gdk.Color SEARCH_ENTRY_DEFAULT_BG = new Gtk.Entry().Style.Base(Gtk.StateType.Normal);
        public static readonly Gdk.Color SEARCH_ENTRY_RED_BG = new Gdk.Color(255, 192, 192);
        public static readonly Gdk.Color SEARCH_ENTRY_YELLOW_BG = new Gdk.Color(255, 255, 128);
        public static readonly Gdk.Color SEARCH_ENTRY_GREEN_BG = new Gdk.Color(192, 255, 192);
        public static readonly Gdk.Color SEARCH_ENTRY_GRAY_BG = new Gdk.Color(192, 192, 192);
    
    }
}
