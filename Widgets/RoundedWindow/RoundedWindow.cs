﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Drawing;
using System.IO;

using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{

    public class RoundedWindow : Gtk.Window
    {
        private readonly int radius;

        protected RoundedWindow(int radius) : base(Gtk.WindowType.Popup)
        {
            this.radius = radius;
            this.SizeRequested += (o, args) => this.UpdateShape();
        }

        private void UpdateShape()
        {
            if (this.Requisition.Width < 1 || this.Requisition.Height < 1)
                return;

            Bitmap bmp = this.CreateShape(this.Requisition);
            MemoryStream stream = new MemoryStream();
            bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            stream.Position = 0;
            Gdk.Pixbuf buf = new Gdk.Pixbuf(stream, bmp.Width, bmp.Height);
            buf.RenderPixmapAndMask(out Gdk.Pixmap map1, out Gdk.Pixmap map2, 255);
            this.ShapeCombineMask(map2, 0, 0);
        }

        private Bitmap CreateShape(Requisition req)
        {
            int r = this.radius;

            Bitmap bmp = new Bitmap(req.Width, req.Height);
            Graphics g = Graphics.FromImage(bmp);
            Brush b = new SolidBrush(Color.Red);
            g.FillEllipse(b, 0, 0, 2*r, 2*r);
            g.FillEllipse(b, req.Width-2*r, 0, 2*r, 2*r);
            g.FillEllipse(b, 0, req.Height-2*r, 2*r, 2*r);
            g.FillEllipse(b, req.Width-2*r, req.Height-2*r, 2*r, 2*r);
            g.FillRectangle(b, r, 0, req.Width-2*r, req.Height);
            g.FillRectangle(b, 0, r, req.Width,     req.Height-2*r);

            return bmp;
        }
    }
}