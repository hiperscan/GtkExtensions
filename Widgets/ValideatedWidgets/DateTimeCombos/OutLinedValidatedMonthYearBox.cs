﻿using System;
using System.Globalization;

namespace Hiperscan.GtkExtensions.Widgets
{
    public class OutLinedValidatedMonthYearBox : OutLinedValidatedWidget
    {
        public OutLinedValidatedMonthYearBox(Gtk.HBox hbox, int start_year, int years, CultureInfo culture) : base (hbox)
        {
            this.InitMonthBox(culture);
            this.InitYearBox(start_year, years);
            this.Refresh();
        }

        protected override void Init()
        {
            base.Init();

            this.MonthCombobox = (Gtk.ComboBox)((Gtk.HBox)this.Parent).Children[0];
            this.YearCombobox = (Gtk.ComboBox)((Gtk.HBox)this.Parent).Children[2];

            this.MonthCombobox.Changed += (sender, e) => this.Refresh();
            this.YearCombobox.Changed += (sender, e) => this.Refresh();

            this.Background = (Gtk.EventBox)((Gtk.HBox)this.Parent).Children[1];
        }

        private void InitMonthBox(CultureInfo culture)
        {
            if (this.MonthCombobox.Model == null)
            {
                Gtk.ListStore store = new Gtk.ListStore(typeof(string));
                Gtk.CellRendererText text = new Gtk.CellRendererText();
                this.MonthCombobox.Clear();
                this.MonthCombobox.PackStart(text, false);
                this.MonthCombobox.SetAttributes(text, "text", 0);
                this.MonthCombobox.Model = store;
            }

            this.MonthCombobox.AppendText("-");
            DateTime date = DateTime.MinValue;
            for (int i = 0; i < 12; i++)
                this.MonthCombobox.AppendText(date.AddMonths(i).ToString("MMMM", culture));

            this.MonthCombobox.Active = 0;
        }

        private void InitYearBox(int start, int years)
        {
            this.StartYear = start;
            this.EndYear = start + years - 1;

            if (this.YearCombobox.Model == null)
            {
                Gtk.ListStore store = new Gtk.ListStore(typeof(string));
                Gtk.CellRendererText text = new Gtk.CellRendererText();
                this.YearCombobox.Clear();
                this.YearCombobox.PackStart(text, false);
                this.YearCombobox.SetAttributes(text, "text", 0);
                this.YearCombobox.Model = store;
            }

            this.YearCombobox.AppendText("-");
            DateTime date = DateTime.MinValue;
            for (int i = start; i < start + years; i++)
                this.YearCombobox.AppendText(i.ToString());

            this.YearCombobox.Active = 0;
        }

        protected override bool Check()
        {
            return (this.MonthCombobox.Active != 0) && (this.YearCombobox.Active != 0);
        }

        public bool SetDate(DateTime date)
        {
            this.Reset();

            int year = date.Year;
            if (year < this.StartYear || year > this.EndYear)
                return false;

            this.YearCombobox.Active = year - this.StartYear + 1;
            this.MonthCombobox.Active = date.Month;
            return true;
        }

        public void Reset()
        {
            this.MonthCombobox.Active = 0;
            this.YearCombobox.Active = 0;
        }

        public Gdk.Color BgColor
        {
            set
            {
                this.Background.ModifyBg(Gtk.StateType.Normal, value);
            }
        }

        public bool Sensitive
        {
            set
            {
                this.YearCombobox.Sensitive = value;
                this.MonthCombobox.Sensitive = value;
            }
        }



        private Gtk.HBox GtkHBox { get; set; }
        public Gtk.ComboBox MonthCombobox { get; set; }
        public Gtk.ComboBox YearCombobox { get; set; }
        private Gtk.EventBox Background { get; set; }

        private int StartYear { get; set; } = -1;
        private int EndYear { get; set; } = -1;


    }
}
