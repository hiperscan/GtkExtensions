﻿using System;
namespace Hiperscan.GtkExtensions.Widgets
{
    public class MonthYearRegexCombobox : RegexWidget
    {
        public MonthYearRegexCombobox(Gtk.HBox hbox, int start_year, int number_years)
        {
            this.GtkHBox = hbox;
            this.Init();
            this.Refresh();
            this.MonthCombobox.Changed += (sender, e) => this.Refresh();
            this.YearCombobox.Changed += (sender, e) => this.Refresh();
            this.GtkContainer.ShowAll();
        }

        protected override void Init()
        {
            this.MonthCombobox = (Gtk.ComboBox) this.GtkHBox.Children[0];
            this.YearCombobox = (Gtk.ComboBox)this.GtkHBox.Children[1];
        }

        protected override void ActivateOkState()
        {
            throw new NotImplementedException();
        }

        protected override void ActivateErrorState()
        {
            throw new NotImplementedException();
        }


        protected override bool RegexIsMatch
        {
            get
            {
                string month = this.MonthCombobox.ActiveText;
                string year = this.YearCombobox.ActiveText;
            }
        }

        public override string ErrorMessage
        {
            set
            {

            }
        }

        private Gtk.HBox GtkHBox { get; set; }
        public Gtk.ComboBox MonthCombobox { get; set; }
        public Gtk.ComboBox YearCombobox { get; set; }
    }
}
