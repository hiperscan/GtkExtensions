﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using System.Text.RegularExpressions;
using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public class OutLinedRegexLabel : OutLinedValidatedWidget
    {
        public OutLinedRegexLabel(Gtk.Label label) : base(label)
        {

        }

        protected override void Init()
        {
            base.Init();

            Gtk.Container parent = this.Parent.Parent as Gtk.Container;
            parent.Remove(this.Parent);

            this.BackGroundEventbox.Add(this.Parent);
            parent.Add(this.BackGroundEventbox);

            this.BgColor = ApoIdentColors.WHITE;
        }

        protected override bool Check()
        {
            if (this.GtkLabel.Text.ToLower() == this.place_holder_text.ToLower())
            {
                if (string.IsNullOrEmpty(this.GtkLabel.Text) == false)
                    return false;
            }

            if (this.Regex == null)
                return true;

            return this.Regex.Match(this.GtkLabel.Text).Success;
        }

        public Gdk.Color BgColor 
        { 
            set
            {
                this.BackGroundEventbox.ModifyBg(StateType.Normal, value);
            }
        } 

        public string Text
        {
            set
            {
                this.GtkLabel.Text = value;
                this.Refresh();
            }
        }

        private string place_holder_text = String.Empty;
        public string PlaceHolderText
        {
            get
            {
                return this.place_holder_text;
            }

            set
            {
                this.place_holder_text = (value ?? string.Empty).Trim();
                this.Refresh();
            }
        }

        private Gtk.Label GtkLabel
        {
            get
            {
                return (Gtk.Label) this.Parent;
            }
        }

        private Gtk.EventBox BackGroundEventbox { get; set; } = new EventBox();
        public Regex Regex { get; set; } = null;
    }
}
