﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using Hiperscan.Unix;

namespace Hiperscan.GtkExtensions.Widgets
{
    public abstract class ImagedValidatedWidget : ValidatedWidget
    {
        protected ImagedValidatedWidget(Gtk.Widget parent_widget) : base(parent_widget)
        {

        }

        protected override void Init()
        {
            Gtk.Container parent = this.Parent.Parent as Gtk.Container;
            parent.Remove(this.Parent);

            Gtk.HBox hbox = new Gtk.HBox(false, 5);
            parent.Add(hbox);

            Gtk.Box.BoxChild box_child = null;

            hbox.Add(this.Parent);
            box_child = (Gtk.Box.BoxChild)(hbox[this.Parent]);
            box_child.Position = 0;
            box_child.Expand = true;

            this.OkIcon = Gtk.Stock.Ok;
            this.OkImage.NoShowAll = true;
            this.OkBox = new Gtk.EventBox();
            this.OkBox.VisibleWindow = false;
            this.OkBox.Add(this.OkImage);
            hbox.Add(this.OkBox);
            box_child = (Gtk.Box.BoxChild)(hbox[this.OkBox]);
            box_child.Position = 1;
            box_child.Expand = false;
            box_child.Fill = false;

            this.ErrorIcon = Gtk.Stock.DialogWarning;
            this.ErrorImage.NoShowAll = true;
            this.ErrorBox = new Gtk.EventBox();
            this.ErrorBox.VisibleWindow = false;
            this.ErrorBox.Add(this.ErrorImage);
            hbox.Add(this.ErrorBox);
            box_child = (Gtk.Box.BoxChild)(hbox[this.ErrorBox]);
            box_child.Position = 2;
            box_child.Expand = false;
            box_child.Fill = false;
        }

        protected override void ActivateErrorState()
        {
            this.ErrorImage.Visible = true;
            this.OkImage.Visible = false;
        }

        protected override void ActivateOkState()
        {
            this.OkImage.Visible = true;
            this.ErrorImage.Visible = false;
        }

        public string OkIcon
        {
            set
            {
                this.OkImage = new Gtk.Image(value, Gtk.IconSize.Menu);
            }
        }

        public string ErrorIcon
        {
            set
            {
                this.ErrorImage = new Gtk.Image(value, Gtk.IconSize.Menu);
            }
        }

        public override string ErrorMessage
        {
            set
            {
                if (string.IsNullOrEmpty(value) == false)
                    ExtToolTip.SetToolTip(this.ErrorBox, Catalog.GetString("Error"), value, Gtk.Stock.DialogWarning);
                else
                    ExtToolTip.UnsetToolTip(this.ErrorBox);
            }
        }

        private Gtk.Image OkImage { get; set; }
        private Gtk.EventBox OkBox { get; set; }
        private Gtk.Image ErrorImage { get; set; }
        private Gtk.EventBox ErrorBox { get; set; }
    }
}
