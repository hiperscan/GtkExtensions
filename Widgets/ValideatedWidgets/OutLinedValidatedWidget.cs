﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


namespace Hiperscan.GtkExtensions.Widgets
{
    public abstract class OutLinedValidatedWidget : ValidatedWidget
    {
        protected OutLinedValidatedWidget(Gtk.Widget parent_widget) : base(parent_widget)
        {

        }

        protected override void Init()
        {
            Gtk.Container parent = this.Parent.Parent as Gtk.Container;
            parent.Remove(this.Parent);

            Gtk.Alignment alignment = new Gtk.Alignment(0.5f, 0.5f, 1.0f, 1.0f);
            alignment.BorderWidth = 2;
            alignment.Add(this.Parent);

            this.Border.Add(alignment);
            parent.Add(this.Border);
        }

        protected override void ActivateOkState()
        {
            if (this.WidgetIsFocused == false)
                this.Border.ModifyBg(Gtk.StateType.Normal, this.ok_color);
            else
                this.Border.ModifyBg(Gtk.StateType.Normal, this.ok_focused_color);

            this.Parent.HasTooltip = false;
        }

        protected override void ActivateErrorState()
        {
            if (this.WidgetIsFocused == false)
                this.Border.ModifyBg(Gtk.StateType.Normal, this.error_color);
            else
                this.Border.ModifyBg(Gtk.StateType.Normal, this.error_focused_color);

            this.Parent.HasTooltip = string.IsNullOrEmpty(this.Parent.TooltipText) == false;
        }

        private Gdk.Color ok_color = ApoIdentColors.OK_BORDER;
        public Gdk.Color OkColor
        {
            set
            {
                this.ok_color = value;
                this.Refresh();
            }
        }

        private Gdk.Color ok_focused_color = ApoIdentColors.OK_FOCUSED_BORDER;
        public Gdk.Color OkFocusedColor
        {
            set
            {
                this.ok_focused_color = value;
                this.Refresh();
            }
        }

        private Gdk.Color error_color = ApoIdentColors.ERROR_BORDER;
        public Gdk.Color ErrorColor
        {
            set
            {
                this.error_color = value;
                this.Refresh();
            }
        }

        private Gdk.Color error_focused_color = ApoIdentColors.ERROR_FOCUSED_BORDER;
        public Gdk.Color ErrorFocusedColor
        {
            set
            {
                this.error_focused_color = value;
                this.Refresh();
            }
        }

        public uint BorderWidth
        {
            set
            {
                (this.Border.Child as Gtk.Alignment).BorderWidth = value;
            }
        }

        protected virtual bool WidgetIsFocused
        {
            get
            {
                return false;
            }
        }

        public override string ErrorMessage
        {
            set
            {
                this.Parent.TooltipText = value;
                this.Refresh();
            }
        }

        protected Gtk.EventBox Border { get; set; } = new Gtk.EventBox();
    }
}
