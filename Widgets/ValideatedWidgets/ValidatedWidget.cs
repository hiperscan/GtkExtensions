﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2018 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using Hiperscan.Unix;

namespace Hiperscan.GtkExtensions.Widgets
{
    public abstract class ValidatedWidget
    {
        public static readonly string DEFAULT_ERROR_MESSAGE = Catalog.GetString("This input field is mandatory.");

        protected ValidatedWidget(Gtk.Widget parent)
        {
            this.Parent = parent;
            this.Init();
            this.Refresh();
        }

        protected abstract void Init();
        protected abstract void ActivateOkState();
        protected abstract void ActivateErrorState();
        protected abstract bool Check();

        protected void Refresh()
        {
            if (this.IgnoreValidation == true)
                this.IsValid = true;
            else
                this.IsValid = this.Check();

            if (this.IsValid == true)
                this.ActivateOkState();
            else
                this.ActivateErrorState();
        }


        private bool ignore_validation = false;
        public bool IgnoreValidation
        {
            protected get
            {
                return this.ignore_validation;
            }
            set
            {
                this.ignore_validation = value;
                this.Refresh();
            }
        }

        public abstract string ErrorMessage { set; }

        public bool IsValid { get; protected set; } = false;
        protected Gtk.Widget Parent { get; set; }
    }
}
