﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.




using System.Text.RegularExpressions;
using Gtk;

namespace Hiperscan.GtkExtensions.Widgets
{
    public class ImagedRegexEntry : ImagedValidatedWidget
    {
        public ImagedRegexEntry(Gtk.Entry entry) : base(entry)
        {
        }


        protected override void Init()
        {
            base.Init();

            this.GtkEntry.ShadowType = ShadowType.None;
            this.GtkEntry.HasFrame = false;
            this.GtkEntry.FocusInEvent += (o, args) => this.Refresh();
            this.GtkEntry.FocusOutEvent += (o, args) => this.Refresh();
            this.GtkEntry.Changed += (sender, e) => this.Refresh();
        }

        protected override bool Check()
        {
            if (this.Regex == null)
                return true;

            return this.Regex.Match(this.GtkEntry.Text).Success;
        }

        public Gtk.Entry GtkEntry
        {
            get
            {
                return (Gtk.Entry)this.Parent;
            }
        }

        public Regex Regex { get; set; } = null;
    }
}
