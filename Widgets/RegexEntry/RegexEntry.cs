﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2018 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Text.RegularExpressions;
using Gtk;
using Hiperscan.Unix;

namespace Hiperscan.GtkExtensions.Widgets
{
    public class RegexEntry : Gtk.HBox
    {
        public delegate bool RegexEntryCondition(string text);

        public RegexEntry(string ok_image, string error_image) : base(false, 5)
        {
            BoxChild box_child = null;

            this.Entry = new Entry();
            this.Add(this.Entry);
            box_child = (BoxChild)(this[this.Entry]);
            box_child.Position = 0;
            box_child.Expand = true;

            this.OkImage = new Image(ok_image, IconSize.Menu);    
            this.OkImage.NoShowAll = true;
            this.OkBox = new EventBox();
            this.OkBox.VisibleWindow = false;
            this.OkBox.Add(this.OkImage);
            this.Add(this.OkBox);
            box_child = (BoxChild)(this[this.OkBox]);
            box_child.Position = 1;
            box_child.Expand = false;
            box_child.Fill = false;

            this.ErrorImage = new Image(error_image, IconSize.Menu);
            this.ErrorImage.NoShowAll = true;
            this.ErrorBox = new EventBox();
            this.ErrorBox.VisibleWindow = false;
            this.ErrorBox.Add(this.ErrorImage);
            this.Add(this.ErrorBox);
            box_child = (BoxChild)(this[this.ErrorBox]);
            box_child.Position = 2;
            box_child.Expand = false;
            box_child.Fill = false;

            this.Refresh();

            this.Entry.Changed += (sender, e) => this.Refresh();
            this.ShowAll();
        }

        private void Refresh()
        {
            bool is_text_valid = this.IsTextValid;

            if (this.Regex == null || this.IgnoreTextValidation == true)
                this.IsTextValid = true;
            else
                this.IsTextValid = this.Regex.Match(this.Entry.Text).Success;
                
            if (is_text_valid == this.IsTextValid)
                return;

            if (this.IsTextValid == true)
            {
                this.ErrorImage.Visible = false;
                this.OkImage.Visible = true;
            }
            else
            {
                this.OkImage.Visible = false;
                this.ErrorImage.Visible = true;
            }
        }

        private Regex regex = null;
        public Regex Regex       
        { 
            private get
            {
                return this.regex;
            }
            set
            {
                this.regex = value;
                this.Refresh();
            }
        }

        private bool ignore_text_validation = false;
        public bool IgnoreTextValidation
        {
            private get
            {
                return this.ignore_text_validation;
            }
            set
            {
                this.ignore_text_validation = value;
                this.Refresh();
            }
        }

        public string OkMessage     
        { 
            set
            {
                if (string.IsNullOrEmpty(value) == false)
                    ExtToolTip.SetToolTip(this.OkBox, Catalog.GetString("OK"), value, Stock.Yes);
                else
                    ExtToolTip.UnsetToolTip(this.OkBox);
            }
        }

        public string ErrorMessage  
        { 
            set
            {
                if (string.IsNullOrEmpty(value) == false)
                    ExtToolTip.SetToolTip(this.ErrorBox, Catalog.GetString("Error"), value, Stock.DialogWarning);
                else
                    ExtToolTip.UnsetToolTip(this.ErrorBox);
            }
        } 

        public bool IsTextValid             { get; private set; } = false;
        public Gtk.Entry Entry              { get; private set; }
        private Image OkImage               { get; set; }
        private EventBox OkBox              { get; set; }
        private Image ErrorImage            { get; set; }
        private EventBox ErrorBox           { get; set; }



    }
}
