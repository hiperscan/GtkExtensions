﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2017 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Reflection;
using System.IO;
using Gtk;

using Hiperscan.Unix;
using Hiperscan.Common.I18n;


namespace Hiperscan.GtkExtensions.Widgets
{
    
    public enum FinderReferenceType
    {
        ExternalBlack,
        ExternalWhiteZenith,    
        ExternalInsert
    }
        
    public static class FinderReferenceDialog
    {
        public static void Run(Window parent, FinderReferenceType reference_type, int button_height = -1)
        {
            string picure_file = string.Empty;
            string message = string.Empty;

            switch (reference_type)
            {
                case FinderReferenceType.ExternalBlack:
                    message = Catalog.GetString("\n\n\nPlease apply the black reference.");
                    picure_file = "black_reference.png";
                    break;

                case FinderReferenceType.ExternalWhiteZenith:
                    message = Catalog.GetString("\n\n\nPlease apply the Zenith white reference.");
                    picure_file = "white_zenith_reference.png";
                    break;

                case FinderReferenceType.ExternalInsert:
                    message = Catalog.GetString("\n\n\nPlease apply the insert white reference.");
                    picure_file = "white_insert_reference.png";
                    break;

                default:
                    throw new Exception(Catalog.GetString("FinderMessageDialog: Invalid reference type."));
            }

            FinderReferenceDialog.Dialog = new MessageDialog(parent,
                                            DialogFlags.Modal,
                                            Gtk.MessageType.Other,
                                            ButtonsType.None,
                                            message);

            string path = Path.Combine("Widgets", "FinderReferenceDialog", "Resources");
            Gdk.Pixbuf buf = new Gdk.Pixbuf(Locale.ResourceFileRelativeToAssembly(Assembly.GetExecutingAssembly(), path, picure_file, false));
            FinderReferenceDialog.Dialog.Image = new Gtk.Image(buf);
            FinderReferenceDialog.Dialog.Image.Show();
            FinderReferenceDialog.Dialog.WindowPosition = Gtk.WindowPosition.CenterOnParent;
            FinderReferenceDialog.Dialog.AddButton(Gtk.Stock.Cancel, Gtk.ResponseType.Cancel);
            FinderReferenceDialog.Dialog.AddButton(Gtk.Stock.Ok, Gtk.ResponseType.Ok).HasFocus = true;

            if (parent != null && parent.IconList != null && parent.IconList.Length != 0)
                FinderReferenceDialog.Dialog.IconList = parent.IconList;

            if (button_height > 0)
            {
                foreach (Gtk.Widget w in FinderReferenceDialog.Dialog.ActionArea.Children)
                {
                    if (w is Gtk.Button)
                        w.HeightRequest = button_height;
                }
            }

            FinderReferenceDialog.Dialog.KeyPressEvent += KeyPressEvaluation.OnKeyOrScanPressed;

            FinderReferenceDialog.Run();
        }

        private static void Run()
        {
            ResponseType response = (ResponseType)FinderReferenceDialog.Dialog.Run();
            FinderReferenceDialog.Dialog.Destroy();
            FinderReferenceDialog.Dialog = null;

            if (response != ResponseType.Ok && response != ResponseType.None)
                throw new Exception(Catalog.GetString("Recalibration procedure was not finished. " + 
                                                      "Please repeat measurement."));
        }

        public static MessageDialog Dialog { get; private set; } = null;
    }
}

