﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using Hiperscan.GtkExtensions.Compiler;

using Hiperscan.Unix;


namespace Hiperscan.GtkExtensions.Printing
{
    
    public class LinuxPrinting : Printing
    {
        public  LinuxPrinting(string printer, string document, GraphicsSize page_size, GraphicsPoint page_offset, int orientation, PrintDataHandler handle_data)
        {
            this.PrinterName = printer;
            this.DocumentName = document;
            this.HandleData = handle_data;
            this.PageSize = page_size;
            this.PageOffset = page_offset;
            this.Orientation = orientation;
        }
        
        #region implemented abstract members of Hiperscan.GtkExtensions.Printing.Printing
        
        public override void PrintData ()
        {
            Gtk.PrintOperation print = new Gtk.PrintOperation();
            print.JobName = string.IsNullOrEmpty(this.DocumentName) ? Catalog.GetString("Page") : this.DocumentName;
            print.UseFullPage = true;
            
            print.PrintSettings = new Gtk.PrintSettings();
            print.PrintSettings.Quality = Gtk.PrintQuality.High;
            //print.PrintSettings.PaperSize = new Gtk.PaperSize(string.Empty, string.Empty,user_defined_width_px,user_defined_height_px);        
            print.PrintSettings.Printer = this.PrinterName;
            
                    
            print.BeginPrint += (sender, e) =>
            {
                double user_defined_width_px = 0;
                double user_defined_height_px = 0;
                if(this.Orientation / 90 == 0 || this.Orientation / 90 == 2)
                {
                    user_defined_width_px = (this.PageSize.Width + Math.Abs(this.PageOffset.X)) / 25.4f * MonoCairoGraphicCompiler.DPI;
                    user_defined_height_px = (this.PageSize.Height +  Math.Abs(this.PageOffset.Y)) / 25.4f * MonoCairoGraphicCompiler.DPI;
                }
                else
                {
                    user_defined_width_px = (this.PageSize.Width + Math.Abs(this.PageOffset.Y)) / 25.4f * MonoCairoGraphicCompiler.DPI;
                    user_defined_height_px = (this.PageSize.Height +  Math.Abs(this.PageOffset.X)) / 25.4f * MonoCairoGraphicCompiler.DPI;
                }
                
                print.PrintSettings.PaperSize = new Gtk.PaperSize(string.Empty, string.Empty,user_defined_width_px,user_defined_height_px);
                print.NPages = 1;
            };
            
            print.DrawPage += (sender, e) =>
            {
                double width = this.PageSize.Width  / 25.4f * MonoCairoGraphicCompiler.DPI;
                double height = this.PageSize.Height  / 25.4f *MonoCairoGraphicCompiler.DPI;
                
                MonoCairoGraphicCompiler cairo_translator = new MonoCairoGraphicCompiler(e.Context.CairoContext);
                GraphicsObject visual_tree = this.HandleData(new GraphicsRectangle(0 ,0,  width, height), cairo_translator);
                
                GraphicsCompiler graphic_compiler = new GraphicsCompiler(visual_tree, cairo_translator);
                graphic_compiler.TraverseVisualTree();    
                cairo_translator.Dispose();
            };
    
            Gtk.PrintOperationResult result = print.Run(Gtk.PrintOperationAction.Print, null);
            if (result == Gtk.PrintOperationResult.Error)
                throw new Exception(Catalog.GetString("Printer settings are not valid."));
        }
        
        #endregion}
    }
}

