﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
using System.Drawing.Printing;

using Hiperscan.GtkExtensions.Compiler;
using Hiperscan.Unix;


namespace Hiperscan.GtkExtensions.Printing
{
    
    public class WindowsPrinting : Printing
    {
        public  WindowsPrinting(string printer, string document, GraphicsSize page_size, PrintDataHandler handle_data)
        {
            this.PrinterName = printer;
            this.DocumentName = document;
            this.HandleData = handle_data;
            this.PageSize = page_size;
        }
        
        #region implemented abstract members of Hiperscan.GtkExtensions.Printing.Printing
        
        public override void PrintData ()
        {
            PrintDocument pd = new PrintDocument();
            pd.PrinterSettings.PrinterName = this.PrinterName;
            pd.DocumentName = string.IsNullOrEmpty(this.DocumentName) ? Catalog.GetString("Page") : this.DocumentName;
        
            pd.PrintPage += delegate(object sender, PrintPageEventArgs e) 
            {
                double user_defined_width_px = this.PageSize.Width / 25.4f * SystemDrawingGraphicCompiler.DPI;
                double user_defined_height_px = this.PageSize.Height / 25.4f * SystemDrawingGraphicCompiler.DPI;
                
                SystemDrawingGraphicCompiler drawing_tranlator = new  SystemDrawingGraphicCompiler(e.Graphics);
                GraphicsObject visual_tree = this.HandleData(new GraphicsRectangle(0 ,0, user_defined_width_px, user_defined_height_px),drawing_tranlator);
                    
                GraphicsCompiler graphic_compiler = new GraphicsCompiler(visual_tree, drawing_tranlator);
                graphic_compiler.TraverseVisualTree();    
            };
                            
            if (pd.PrinterSettings.IsValid)
                pd.Print();
            else
                throw new Exception(Catalog.GetString("Printer settings are not valid."));
        }
        
        #endregion
    }
}

