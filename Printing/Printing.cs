// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012 Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;

using Hiperscan.GtkExtensions.Compiler;


namespace Hiperscan.GtkExtensions.Printing
{
    
    public abstract class Printing
    {
        public delegate GraphicsObject PrintDataHandler(GraphicsRectangle area, GenericGraphicCompiler translator);
        
        public string PrinterName {get;set;}
        public string DocumentName {get;set;}
        public GraphicsSize PageSize {get;set;}
        public GraphicsPoint PageOffset{get;set;}
        public int Orientation{get;set;}
        
        protected PrintDataHandler HandleData {get;set;}
        
        public abstract void PrintData();
    }
}

