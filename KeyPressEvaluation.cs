﻿// Created with MonoDevelop
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
namespace Hiperscan.GtkExtensions
{
    public static class KeyPressEvaluation
    {
        private const uint MAX_SCAN_TIME = 50;

        [GLib.ConnectBefore]
        public static void OnKeyOrScanPressed(object sender, Gtk.KeyPressEventArgs e)
        {
            e.RetVal = KeyPressEvaluation.IsReturnFromScanner(e);
        }

        public static bool IsReturnFromScanner(Gtk.KeyPressEventArgs e)
        {
            if (e.Event.Key == KeyPressEvaluation.ScannStartKey)
            {
                KeyPressEvaluation.StartScanTime = e.Event.Time;
                KeyPressEvaluation.ScannerCounter = 1;
            }

            if (e.Event.Key == Gdk.Key.Return)
            {
                KeyPressEvaluation.EndScanTime = e.Event.Time;

                uint average = (KeyPressEvaluation.EndScanTime - KeyPressEvaluation.StartScanTime);
                average = average / KeyPressEvaluation.ScannerCounter;

                if (average < KeyPressEvaluation.MAX_SCAN_TIME)
                    return true;
            }
            else
            {
                KeyPressEvaluation.ScannerCounter++;
            }

            return false;
        }

        public static Gdk.Key ScannStartKey { get; set; } = Gdk.Key.numbersign;

        private static uint StartScanTime { get; set; } = 0;
        private static uint EndScanTime { get; set; } = 0;
        private static uint ScannerCounter { get; set; } = 1;
    }
}
